﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SeqToC
{
    class Program
    {
        static int ReadInt16(BinaryReader br)
        {
            byte lsb = br.ReadByte();
            byte msb = br.ReadByte();
            return (((int)msb) << 8) + lsb;
        }

        static int ReadInt32(BinaryReader br)
        {
            byte lsb = br.ReadByte();
            byte lsb1 = br.ReadByte();
            byte lsb2 = br.ReadByte();
            byte msb = br.ReadByte();
            return (((int)msb) << 24) + (((int)lsb2) << 16) + (((int)lsb1) << 8) + lsb;
        }

        static void SaveFSEQToC(BinaryReader br, string outfile, long size)
        {
            string tag = new string(br.ReadChars(4));
            if (tag != "PSEQ" && tag != "FSEQ")
            {
                Console.WriteLine("File tag did not say it was an FSEQ file.");
                return;
            }

            int _frame0Offset = ReadInt16(br);
            byte minorVersion = br.ReadByte();
            byte majorVersion = br.ReadByte();
            int fixedheader = ReadInt16(br); // fixed header length
            int channelsPerFrame = ReadInt32(br);
            int frames = ReadInt32(br);
            int frameMS = ReadInt16(br);
            int universes = ReadInt16(br); // universes
            int universeSize = ReadInt16(br); // universe size
            byte gamma = br.ReadByte();
            byte colourEncoding  = br.ReadByte();
            int fill = ReadInt16(br); // fill
            string mediaFile = "NONE";
            if (_frame0Offset > 28)
            {
                int mediafilenamelength = ReadInt16(br);
                if (mediafilenamelength > 0)
                {
                    ReadInt16(br); // mf
                    mediaFile = new string(br.ReadChars(mediafilenamelength));
                }
            }

            Console.WriteLine("Channels: " + channelsPerFrame.ToString());
            Console.WriteLine("Frames: " + frames.ToString());
            Console.WriteLine("Media file: " + mediaFile);

            StreamWriter sw = new StreamWriter(outfile);

            WriteData(sw, br, channelsPerFrame, channelsPerFrame, frames);

            sw.Close();
        }

        static void WriteData(StreamWriter sw, BinaryReader br, int cpf, int ms, int frames)
        {
            byte[] frameBuffer = new byte[cpf];

            sw.WriteLine("#ifndef PIXEL_DATA_H");
            sw.WriteLine("   #define PIXEL_DATA_H");

            sw.WriteLine("   const uint8_t PixelData[" + frames.ToString() + "][" + ms.ToString() + "] PROGMEM = {");

            for (int i = 0; i < frames; i++)
            {
                if (i != 0)
                {
                    sw.Write("      ,");
                }
                else
                {
                    sw.Write("      ");
                }
                sw.Write("{");

                br.Read(frameBuffer, 0, frameBuffer.Length);

                for (int j = 0; j < ms; j++)
                {
                    if (j != 0)
                    {
                        sw.Write(",");
                    }
                    sw.Write(frameBuffer[j].ToString());
                }

                sw.WriteLine("}");
            }

            sw.WriteLine("};");

            sw.WriteLine("#endif");
        }

        static void SaveESEQToC(BinaryReader br, string outfile, long size)
        {
            string tag = new string(br.ReadChars(4));
            if (tag != "ESEQ")
            {
                Console.WriteLine("File tag did not say it was an ESEQ file.");
                return;
            }
            int frame0Offset = 20;
            int mc = ReadInt32(br); // models count ... should be 1
            int channelsPerFrame = ReadInt32(br);
            int offset = ReadInt32(br);
            int ms = ReadInt32(br); // model size

            int frames = (int) ((size - frame0Offset) / channelsPerFrame);

            Console.WriteLine("Models: " + ms.ToString());
            Console.WriteLine("Channels: " + channelsPerFrame.ToString());
            Console.WriteLine("Model Size: " + ms.ToString());
            Console.WriteLine("Frames: " + frames.ToString());

            StreamWriter sw = new StreamWriter(outfile);

            WriteData(sw, br, channelsPerFrame, ms, frames);

            sw.Close();
        }

        static void SaveToC(string f)
        {
            FileInfo fi = new FileInfo(f);
            string cfile = fi.Directory + "\\" + fi.Name + ".h";

            Console.Write("Converting " + f + " to " + cfile);

            BinaryReader br = new BinaryReader(new FileStream(f, FileMode.Open, FileAccess.Read));

            if (fi.Extension.ToLower() == ".fseq")
            {
                SaveFSEQToC(br, cfile, fi.Length);
            }
            else if (fi.Extension.ToLower() == ".eseq")
            {
                SaveESEQToC(br, cfile, fi.Length);
            }
            else
            {
                Console.WriteLine("ERROR Unsupported file type: " + f);
            }

            br.Close();
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Usage: SeqToC <file1> ... <filen>");
            }
            else
            {
                foreach (string f in args)
                {
                    SaveToC(f);
                }
            }
        }
    }
}
